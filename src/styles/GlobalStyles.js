import { StyleSheet } from "react-native";

const GlobalStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#383838',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        color: "#fff",
        paddingTop: 35,
        width: "100%"
    },
    inner : {
        flex: 1,
        width: "100%",
    },
    header: {
        width: "100%",
        borderBottomWidth: 1,
        borderBottomColor: "#fee",
        padding: 5,
        alignItems: "center"
    },
    footer: {
        padding: 5,
        width: "100%",
        flexDirection: "row",
        justifyContent: "center",
        marginBottom: 10,
    },
    footerVertical: {
        padding: 5,
        width: "100%",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 10,
    },
    body: {
        flex:1,
        width: "100%",
        padding: 5,
    },
    sectionTitle: {
        fontSize: 20,
        fontWeight: "bold",
        color: "#be9fe0"
    },
    scrollBody: {
        width: "100%",
        height: "100%",
        backgroundColor: "#404040",
    },
    bigText: {
        fontSize: 25,
    },
    normalText: {
        fontSize: 18,
        color: "#dedede"
    },

    // para desplegar campos de informacion
    infoFieldContainer: {
        backgroundColor: "#404040",
        padding: 10,
        width: "100%",
        flexDirection: "column",
        marginBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: "#777"
    },
    infoFieldLabel: {
        fontSize: 22,
        fontWeight: "bold",
        color: "#fbe8ff",
        borderBottomWidth: 1,
        borderBottomColor: "#000",
    },
    infoFieldData: {
        fontSize: 18,
        color: "#fbe8ff"
    },

    // para las flat lists de informacion y botones
    basicItemContainer : {
        padding: 10,
        marginBottom: 10,
        borderBottomColor: "#747474",
        borderBottomWidth: 1,
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
    },
    basicItemInfoContainer : {
        flex: 1,
    },
    basicItemInfoTitle : {
        fontWeight: "bold",
        fontSize: 20,
        marginBottom: 5,
        color: "#b8b8b8"
    },
    basicItemInfoText : {
        fontSize: 17,
        color: "#ababab"
    },
    basicItemInfoButton : {
        margin: 5,
    },

    // barras
    topToolBar: {
        flexDirection: "row",
        justifyContent: "flex-end",
        marginBottom: 25
    },

    // error messages
    errorMessages : {
        color: "#ff5252",
        fontSize: 18,
    }
});;

export default GlobalStyles;