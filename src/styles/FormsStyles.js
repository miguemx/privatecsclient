import { StyleSheet } from "react-native";

const FormStyles = StyleSheet.create({
    // containers
    formContainer: {
        flex: 1,
        width: "100%",
    },
    formContainerScrollContent: {
        alignItems: "center", 
        justifyContent: "center", 
        flex: 1,
        padding: 15
    },

    // buttons
    bigButton: {
        width: "100%",
        backgroundColor: "#5b3882",
        padding: 5,
        justifyContent: "center",
        alignItems: "center"
    },
    bigButtonDisabled: {
        width: "100%",
        backgroundColor: "#797979",
        padding: 5,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row"
    },

    bigButtonRed: {
        width: "100%",
        backgroundColor: "#8f0e0e",
        padding: 5,
        justifyContent: "center",
        alignItems: "center"
    },
    textBigButton: {
        fontSize: 19,
        color: "#fff",
        fontWeight: "bold"
    },

    linkButton: {
        padding: 5,
    },
    linkButtonText: {
        color: "#2b80ff",
        textDecorationLine: "underline",
        fontSize: 18,
    },

    bottomButtons:{
        margin: 5,
        backgroundColor: "#5b3882",
        padding: 5,
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    bottomButtonsDisabled:{
        margin: 5,
        backgroundColor: "#625a69",
        padding: 5,
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    bottomButtonsRed:{
        margin: 5,
        backgroundColor: "#8f0e0e",
        padding: 5,
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    bottomButtonsText : {
        fontSize: 19,
        color: "#fff",
        fontWeight: "bold",
        alignItems: "center",
        justifyContent: "center"
    },

    // inputs
    fieldLabel: {
        color: "#fff",
        fontSize: 20,
        margin: 5,
    },
    input: {
        width: "100%",
        borderWidth: 1,
        borderColor: "#cdcdcd",
        borderRadius: 5,
        backgroundColor: "#fff",
        padding: 10,
        marginBottom: 20,
    },

    dropDownContainer: {
        width: "100%",
        borderWidth: 1,
        borderColor: "#cdcdcd",
        borderRadius: 5,
        height: 40,
        marginBottom: 20
    },
    dropDownListStyle: {
        alignItems: "flex-start"
    },
    dropDrowItem: {
        alignItems: "flex-start",
        justifyContent: "flex-start"
    },

    // for credit cards
    formCardContainer : {
        width: "100%",
        padding: 5,
    },
    creditCardContainer : {
        width: "100%",
        flexDirection: "column",
        padding: 5,
    },
    creditCardDataContainer : {
        flexDirection: "row",
    },
    creditCardData: {
        flex: 1,
    },

});

export default FormStyles;