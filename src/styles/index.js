export { default as GlobalStyles } from "./GlobalStyles";
export { default as ServiceStyles } from "./ServiceStyles";

export { default as FormsStyles } from "./FormsStyles";