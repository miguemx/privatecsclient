import Constants from  "expo-constants";
import { StyleSheet } from "react-native";

const ServiceStyles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: "column",
        borderWidth: 0,
        paddingTop: Constants.statusBarHeight,
        paddingRight: 5,
        paddingLeft: 5,
    },

    titleScreen : {
        fontSize: 24,
        color: "#5b3882",
        fontWeight: "bold",
        paddingTop: 20,
        marginBottom: 20,
        borderBottomWidth: 1,
        borderBottomColor: "#dedede"
    },

    map: {
        flex: 1,
    },
    tripsControls: {
        flexDirection: "column",
        padding: 10,
        height: 80,
        zIndex: 1000,
    },
    inputService : {
        margin: 5,
        padding: 5,
        borderColor: "#dedede",
        borderWidth: 1,
        borderRadius: 5,
    },

    searcher: {
        paddingTop: 10,
        flex: 2,
        backgroundColor: "#dedede",
    },
    mapSearcher: {
        flex: 1
    },
    questionTop: {
        fontSize: 24,
        color: "#be9fe0",
        fontWeight: "bold",
        paddingTop: 20,
    },

    tripOverviewItem : {
        borderWidth: 2,
        borderColor: "#be9fe0",
        borderRadius: 5,
        padding: 10,
        marginBottom: 15,
    },
    tripOverviewList : {
        borderWidth: 2,
        borderColor: "#be9fe0",
        borderRadius: 5,
        padding: 10,
        marginBottom: 15,
        flex: 1,
    },
    tripOverviewItemRow : {
        flexDirection: "row",
    },

    touchableInputLabel: {
        fontSize: 20,
        color: "#be9fe0",
        marginBottom: 5,
        flex: 1,
    },
    touchableInputLabelx: {
        fontSize: 20,
        color: "#be9fe0",
        marginBottom: 5,
    },
    touchableInputText: {
        fontSize: 19,
        color: "#575757"
    },
    touchableInput : {
        borderWidth: 1,
        borderColor: "#797979",
        backgroundColor: "#fff",
        padding: 5,
        color: "#565656",
        borderRadius: 5,
        marginBottom: 10,
    },
    buttonItemsContainer : {
        justifyContent: "flex-end",
        flexDirection: "row"
    },
    buttonItem:{
        marginLeft: 5,
    },

    carListItem : {
        backgroundColor: "#be9fe0",
        borderWidth: 1,
        borderColor: "#be9fe0",
        borderRadius: 5,
        flexDirection: "row",
        margin: 5,
        padding: 5,
        alignItems: "center",
    },
    carListItemSelected : {
        backgroundColor: "#5b3882",
        borderWidth: 3,
        borderColor: "#be9fe0",
        borderRadius: 5,
        flexDirection: "row",
        margin: 5,
        padding: 5,
        alignItems: "center",
    },
    carListName: {
        color: "#5b3882",
        fontSize: 18,
        flex: 1,
        alignItems: "flex-start",
        marginLeft: 10,
    },
    carListNameSelected: {
        color: "#be9fe0",
        fontSize: 18,
        flex: 1,
        alignItems: "flex-start",
        marginLeft: 10,
    },
    carListPrice: {
        color: "#5b3882",
        fontSize: 18,
        alignItems: "flex-end",
    },
    carListPriceSelected: {
        color: "#be9fe0",
        fontSize: 18,
        alignItems: "flex-end",
    },

    califContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        padding: 10,
    },

    califQuestion: {
        fontSize: 20,
        color: "#323232",
        marginBottom: 20,
        alignItems: "center",
        textAlign: "center",
        justifyContent: 'center',
    },

    califStarsContainer: {
        flexDirection: "row"
    },

    califEmoji: {
        margin: 5,
    },


});

export default ServiceStyles;