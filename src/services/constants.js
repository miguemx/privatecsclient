// -----------------------------------------------------------------------|
// URLS para los web services                                             |
// -----------------------------------------------------------------------|

const UrlDomain = "http://api.privatecarservise.com/";
// const UrlDomain = "http://localhost/mexicana-api/public/";

export const UrlLogin = UrlDomain + "Login/Login";
export const UrlRegisterUser = UrlDomain + "Users/Crea";

export const UrlRequestTrip = UrlDomain + "Calls/Crea";
export const UrlCarros = UrlDomain + "Calls/Estima";

export const UrlUserProfile = UrlDomain + "Users/Ver/";

export const UrlCards = UrlDomain + "Customers/Cards/";
export const UrlCardAdd = UrlDomain + "PaymentMethods/AddCard";
export const UrlCardDefault = UrlDomain + "PaymentMethods/MakeCardDefault";
export const UrlCardRemove = UrlDomain + "PaymentMethods/DeleteCard";

export const UrlScore = UrlDomain + "Users/Califica";

export const UrlSocket = "http://migue.com.mx";


// -----------------------------------------------------------------------|
// Textos de meses y dias, y calculo de años                              |
// -----------------------------------------------------------------------|
export const Months = [
    { key: "1", value: "1", label: "January" },
    { key: "2", value: "2", label: "February" },
    { key: "3", value: "3", label: "March" },
    { key: "4", value: "4", label: "April" },
    { key: "5", value: "5", label: "May" },
    { key: "6", value: "6", label: "June" },
    { key: "7", value: "7", label: "July" },
    { key: "8", value: "8", label: "August" },
    { key: "9", value: "9", label: "September" },
    { key: "10", value: "10", label: "October" },
    { key: "11", value: "11", label: "November" },
    { key: "12", value: "12", label: "December" },
];

export const Years = calculaAnios(10);



export const IconCar = require("../../assets/icons/car-sedan.png");
export const IconCarSedan = require("../../assets/icons/car-jeepeta.png");
export const IconCarMinivan = require("../../assets/icons/car-minivan.png");
export const IconCarSuv = require("../../assets/icons/car-suv.png");


// -----------------------------------------------------------------------|
// funciones de exportables como helpers                                  |
// -----------------------------------------------------------------------|
/**
 * 
 * @param {string} url la URL que se va a llamar
 * @param {string} method el metodo de la llamada POST | GET
 * @param {object} parameters un objeto con el body a enviar para la peticion (null cuando se trate de una peticion GET)
 * @param {boolean} returnError indica si el error se regresa de la llamada a la funcion o en su lugar se regresa false
 * @return {object} response la respuesta que se manda 
 * @return {boolean} si returnError es true, entonces se devuelve el error en lugar de false
 */
export const CallApi = async (url, method, parameters, returnError) => {
    let result = false;
    method = method.toUpperCase();
    let request = { method: method, headers: { "Content-Type": "application/json" } };
    if ( method === 'POST' && parameters !== null ) {
        request.body = JSON.stringify(parameters);
    }
    let response = await fetch(url, request)
        .then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => {return response });

    if ( typeof(response) === "object" && response !== null ) {
        if ( response.status === "ok" ) {
            result = response;
        }
        else {
            result = ( returnError === null )? false: response.message;    
        }
    }
    else {
        result = ( returnError === null )? false: "Cannot handle response from server.";
    }
    return result;
}




// -----------------------------------------------------------------------|
// funciones de ayuda                                                     |
// -----------------------------------------------------------------------|
/**
 * obtiene el numero de anios a partir del actual y regresa un objeto que pueda ser renderizable
 * por componentes react
 * @param {int} numYears el numero de anios a obtener 
 * @return {array} un arreglo de objetos con los anios renderizables por react
 */
function calculaAnios (numYears) {
    let currDate = new Date();
    let currYear = currDate.getFullYear();
    let years = new Array();
    for ( let i=0; i<=numYears; i++ ) {
        let yearValue = currYear + i;
        years.push({
            key: yearValue,
            label: yearValue,
            value: yearValue
        });
    }
    return years;
}





