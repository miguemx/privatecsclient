export function getInfoAddress( addressComponents ) {
    let address = new Object();
    
    address.streetNumber = searchFieldAddress(addressComponents,'street_number');
    address.street       = searchFieldAddress(addressComponents,'route');
    address.city         = (searchFieldAddress(addressComponents,'locality'))?searchFieldAddress(addressComponents,'locality'):searchFieldAddress(addressComponents,'administrative_area_level_2');
    address.state        = searchFieldAddress(addressComponents,'administrative_area_level_1');
    address.country      = searchFieldAddress(addressComponents,'country');
    address.postalCode   = searchFieldAddress(addressComponents,'postal_code');
    address.neighborhood = (searchFieldAddress(addressComponents,'neighborhood'))?searchFieldAddress(addressComponents,'neighborhood'):searchFieldAddress(addressComponents,'sublocality_level_1');
    
    return address;
}

function searchFieldAddress(address,field) {
    var name = '';
    for ( var i=0; i<address.length; i++ ) {
        for ( var j=0; j<address[i].types.length; j++ ) {
            if ( address[i].types[j] == field ) {
                name = address[i].long_name;
                break;
            }
        }
    }
    return name;
}