import React from "react";

import { Text, View, TouchableOpacity, Alert, ActivityIndicator, Image } from "react-native";
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import { ScrollView } from "react-native-gesture-handler";

import { ServiceStyles, FormsStyles, GlobalStyles } from "../styles";
import { MyMapView, LocationPicker, TripOverviewItem, CarSelector, Calificador } from "../components";
import { getLocation } from "../services/location-services"

import { UrlSocket, UrlRequestTrip, UrlCarros } from "../services/constants";

// const io = require('socket.io-client');
// const socket = io( UrlSocket, { transports: ['websocket'], });

const carTypes = [
    { label: "Sedan", value:"1" },{ label: "Minivan", value:"2" },{ label: "SUV", value:"3" }
];

export default class ServiceScreen extends React.Component {

    state = {
        client: null,
        carType: "1",

        pickup: {},
        pickupPicker: false,
        pickupText: "-- Select pickup location --",

        destination: {},
        destinationPicker: false,
        destinationText: "-- Select your destination --",
        
        tripAmount: '0.0',
        tripEta: '0 min',
        tripDistance: '0 km',
        
        status: "idle",
        requesting: false,

        tripData: null,
        cars: [],
        findingcars: false,

        tmplatitudes : "",
        calificando: false
    }

    /**
     * componentDidMount
     */
    componentDidMount() {
        this.setState({client: this.props.client});
        this.setSocketConnection();
        this.getInitialLocation();

        const { navigation } = this.props;
        this.focusListener = navigation.addListener("focus", () => {      
               
        });
    }

    componentDidUpdate() {
        if (  typeof(this.state.pickup.latitude) == 'number' && typeof(this.state.destination.latitude) == 'number' ) {
            let tmp = this.state.pickup.latitude + '-' + this.state.destination.latitude;
            if ( this.state.tmplatitudes !== tmp ) {
                this.getCars();
                this.setState({tmplatitudes: tmp});
            }
        }
    }

    /**
     * obtiene la localizacion incial del dispositivo para ponerla en el mapa
     */
    getInitialLocation() {
        getLocation().then(
            (data) => {
                this.setState({
                    region: {
                        latitude: data.latitude,
                        longitude: data.longitude,
                        latitudeDelta: 0.003,
                        longitudeDelta: 0.003
                    }
                });
            }
        );
    }

    /**
     * Actualiza el mapa mostrado al lugar escrito
     * @param {object} region 
     */
    onMapRegionChange(region) {
        this.setState({ region });
    }

    /**
     * sets socket connection to central server
     * on connect sets connection
     * on newcall receives new trip data and fires renderNewTrip
     */
    setSocketConnection() {
        // socket.on('client', (data) => { 
        //     this.comunicate(data);
        // });
    }

    /**
     * abre un dialogo modal con el selector de ubicacion
     * @param {string} type el tipo de selector a abrir
     * @param {boolean} open indica si se debe abrir (true) o cerrar(false)
     */
    openLocation = (type, open) => {
        if ( type === 'pickup' ) {
            this.setState({pickupPicker: open});
        }
        if ( type === 'destination' ) {
            this.setState({destinationPicker: open});
        }
    }

    /**
     * pone en el estado la informacion de la localizacion, ya sea origan o destino para conocer la ruta
     * @param {string} tipo el tipo de localizacion a poner en el estado (pickup, dest) 
     * @param {object} data la informacion para settear
     */
    setLocation = (tipo, data) => {
        let text = ( data.name )? data.name : "Selected location on map";
        if ( tipo == 'pickup' ) {
            this.setState({
                pickup: data,
                pickupPicker: false,
                pickupText: text,
            });
        }
        else if ( tipo == 'destination' ) {
            this.setState({
                destination: data,
                destinationPicker: false,
                destinationText: text,
            });
        }
    }

    /**
     * quita los valores del estado de una location para permitir al usuario cambiarla
     * @param {string} tipo el tipo de location a remover de los estados
     */
    removeLocation = (tipo) => {
        if ( tipo == 'pickup' ) {
            this.setState({
                pickup: {},
                pickupText: "-- Select pickup location --",
            });
        }
        else if ( tipo == 'destination' ) {
            this.setState({
                destination: {},
                destinationText: "-- Select your destination --",
            });
        }
    }

    /**
     * busca en la API la lista de carros y precios de acuerdo a los datos que el cliente introduce
     * para recogida y destino
     */
    getCars = async () => {
        this.setState({findingcars: true});
        let data = {
            client: this.state.client,
            pickupMapsCoord: this.state.pickup.latitude + ',' + this.state.pickup.longitude,
            destMapsCoord: this.state.destination.latitude + ',' + this.state.destination.longitude
        };
        let response = await fetch(UrlCarros, {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(data), // data can be `string` or {object}!
                headers:{
                    'Content-Type': 'application/json'
                }
        }).then(res => res.json()).catch(error => console.log('Error:', error)).then(response => { return response });
        
        if ( typeof(response) == 'object' ) {
            if ( response.status === 'ok' ) {
                this.setState({ cars: response.data });
            }
            else {
                Alert.alert( "Private Car Service", response.message );
            }
        }

        this.setState({findingcars: false});
    }

    /**
     * selecciona entre establecer la direccion origen,destino o pedir el viaje cuando ambas estan listas
     */
    requestTrip = () => {
        if( Object.keys(this.state.pickup).length === 0) {
            Alert.alert("Private Car Service","Please select your pick up location.");
        }
        else if ( Object.keys(this.state.destination).length === 0 ) {
            Alert.alert("Private Car Service","Please select your destination.");
        }
        else {
            if ( this.state.carType !== null ) {
                this.setState({requesting: true});
                this.registerTrip();
            }
            else {
                Alert.alert("Private Car Service","Please select a car type.");
            }
        }
    }

    /**
     * crea un registro de viaje y envia al socket la informacion para solicitar el chofer
     */
    registerTrip = async () => {
        let data = this.getTripData();
        let response = await fetch(UrlRequestTrip, {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(data), // data can be `string` or {object}!
                headers:{
                    'Content-Type': 'application/json'
                }
        }).then(res => res.json()).catch(error => console.error('Error:', error)).then(response => {return response });
        
        if ( typeof(response) == 'object' ) {
            if ( response.status === 'ok' ) {
                response.data.nivel = 1;
                socket.emit("newcall", response.data);
                this.setState({
                    status: "requested", 
                    tripAmount: response.data.amount, 
                    tripDistance: response.data.distance, 
                    tripEta: response.data.eta,
                    requesting: false,
                    tripData: response.data,
                });
            }
            else {
                Alert.alert( "Private Car Service", response.message );
                
                this.setState({
                    status: "idle", 
                    requesting: false,
                });
            }
        }
    }

    /**
     * genera la informacion que sera enviada a la aPI para crear el viaje, a partir del estado
     */
    getTripData = () => {
        let data = {
            carType : this.state.carType,
            pickupMapsId : this.state.pickup.placeId,
            pickupMapsCoord : this.state.pickup.latitude + ',' + this.state.pickup.longitude,
            pickupMapsName : this.state.pickup.name,
            pickupNumber : this.state.pickup.number,
            pickupStreet : this.state.pickup.street,
            pickupCity : this.state.pickup.city,
            pickupState : this.state.pickup.state,
            pickupZip : this.state.pickup.zip,
            pickupCountry : this.state.pickup.country,
            
            destMapsId : this.state.destination.placeId,
            destMapsCoord : this.state.destination.latitude + ',' + this.state.destination.longitude,
            destMapsName : this.state.destination.name,
            destNumber : this.state.destination.number,
            destStreet : this.state.destination.street,
            destCity : this.state.destination.city,
            destState : this.state.destination.state,
            destZip : this.state.destination.zip,
            destCountry : this.state.destination.country,
            
            client : this.state.client,
            registerType: "APP"
        };
        return data;
    }

    confirmCancel = () => {
        Alert.alert(
            "Private Car Service",
            "Are sure you want to cancel current trip request?\nYou will be billed for cancellation depending on your driver's status.",
            [
                { text: "OK", onPress: () => this.cancelTrip() },
                { text: "Cancel", onPress: () => {} }
            ],
            { cancelable: false }
        );
    }

    /**
     * cancela el viaje actual
     */
    cancelTrip = () => {

        // socket.emit("client", {client: {id: 1}});
       
        this.setState({
            carType: "1",

            pickup: {},
            pickupPicker: false,
            pickupText: "-- Select pickup location --",

            destination: {},
            destinationPicker: false,
            destinationText: "-- Select your destination --",
            
            tripAmount: '0.0',
            tripEta: '0 min',
            tripDistance: '0 km',
            
            status: "idle",
            requesting: false,

            tripData: null,
        });
    }

    /**
     * selecciona las acciones para realizar comunicacion con el cliente
     * @param {object} data  informacion que viene del socket
     */
    comunicate = (data) => {
        if ( data.client == this.state.client ) {
            if ( data.action == 'notify' ) {
                this.renderNotification( data.data );
            }
            if ( data.action == 'endtrip' ) {
                this.endTrip( data );
            }
        }
    }

    /**
     * manda un alert con el mensaje proporcionado
     * @param {string} message el mensaje
     */
    renderNotification = (message) => {
        Alert.alert(
            "Private Car Service",
            message,
            [
              { text: "OK", onPress: () => {} }
            ],
            { cancelable: false }
          );
    }

    /**
     * finaliza el viaje actual
     * @param {object} data 
     */
    endTrip = (data) => {
        this.setState({
            pickup: {},
            pickupPicker: false,
            pickupText: "-- Select pickup location --",
            destination: {},
            destinationPicker: false,
            destinationText: "-- Select your destination --",
            tripAmount: '0.0',
            tripEta: '0 min',
            tripDistance: '0 km',
            status: "idle",
            requesting: false,
            tripData: null,
            calificando: true,
        });
        this.getInitialLocation();
    }

    /**
     * pone en los datos del viaje, el tipo de carro seleccionado por el usuario
     * @param {sting} idType el ID del tipo de carro 
     */
    setCarType = (idType) => {
        this.setState({carType: idType});
    }

    /**
     * render muestra la pantalla de seleccion de viaje, o en su defecto la pantalla de viaje activo
     */
    render() {
        if ( this.state.status === "idle" ) {
            return(
                <View style={GlobalStyles.container}>
                    <View style={GlobalStyles.header}>
                        <Text style={GlobalStyles.sectionTitle}>Trip Overview</Text>
                    </View>
                    <View style={GlobalStyles.body}>
                            <TripOverviewItem 
                                title="Plese pick me up at" inputText={this.state.pickupText} tipo="pickup"
                                openLocation={this.openLocation} removeLocation={this.removeLocation}
                            />
                            <TripOverviewItem 
                                title="Plese take me to" inputText={this.state.destinationText} tipo="destination"
                                openLocation={this.openLocation} removeLocation={this.removeLocation}
                            />
                            
                            <View style={ServiceStyles.tripOverviewList}>
                                <Text style={ServiceStyles.touchableInputLabelx}>Select car type</Text>
                                {
                                    (this.state.findingcars === false) ?
                                    <CarSelector cars={this.state.cars} selectCarType={this.setCarType} selected={this.state.carType} />
                                    :
                                    <ActivityIndicator size="large" />
                                }
                            </View>
                    </View>
                    
                    <View style={GlobalStyles.footer}>
                        {
                            (this.state.requesting === false ) ?
                            <TouchableOpacity
                                style={FormsStyles.bigButton}
                                onPress={this.requestTrip}>
                                <Text style={FormsStyles.textBigButton}>Request trip</Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity
                                style={FormsStyles.bigButtonDisabled}
                                onPress={this.disabled}>
                                <ActivityIndicator />
                                <Text style={FormsStyles.textBigButton}>Requesting trip</Text>
                            </TouchableOpacity>
                        }

                        { 
                        // -- Un location picker nunca debe ir dentro de un scroll view 
                        }
                        <LocationPicker 
                            visible={this.state.pickupPicker} 
                            title="Where do we pick you up?"
                            setLocation={this.setLocation}
                            tipo="pickup" />
                        <LocationPicker 
                            visible={this.state.destinationPicker} 
                            title="Where are you going?"
                            setLocation={this.setLocation}
                            tipo="destination" />
                        {
                            // -- fin de los location picker 
                        }
                        <Calificador visible={this.state.calificando} success={()=>{this.setState({calificando: false})}} />
                    </View>
                </View>
            );
        }
        else if ( this.state.status === "requested" ) {
            return(
                <View style={ServiceStyles.container}>
                    <MyMapView
                        region={this.state.region}
                        onRegionChange={(reg) => this.onMapRegionChange(reg)} />
                    
                    <Text style={ServiceStyles.questionTop}>You are on your way.</Text>
                    
                    <Text >Amount: $ {this.state.tripAmount}</Text>
                    <Text >Distance: {this.state.tripDistance} km.</Text>
                    <Text >ETA: {this.state.tripEta}.</Text>
                    <View style={ServiceStyles.tripsControls}>
                        <TouchableOpacity
                            style={FormsStyles.bigButton}
                            onPress={this.confirmCancel}>
                            <Text style={FormsStyles.textBigButton}>Cancel Trip</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            );
        }
        else if ( this.state.status === "ontrip" ) {
            return(
                <View style={ServiceStyles.container}>
                    <MyMapView
                        region={this.state.region}
                        onRegionChange={(reg) => this.onMapRegionChange(reg)} />
                    
                    <Text style={ServiceStyles.questionTop}>You are on your way!</Text>
                    
                    <Text >Amount: $ {this.state.tripAmount}</Text>
                    <Text >Distance: {this.state.tripDistance} km.</Text>
                    <Text >ETA: {this.state.tripEta}.</Text>
                    <View style={ServiceStyles.tripsControls}>
                        <TouchableOpacity
                            style={FormsStyles.bigButtonDisabled}
                            onPress={this.disabled}>
                            <Text style={FormsStyles.textBigButton}>Cancel Trip</Text>
                        </TouchableOpacity>
                    </View>
                    
                </View>
            );
        }
    }

}

