import React from "react";

import { Text, ScrollView, View, SafeAreaView, TouchableOpacity, Button } from "react-native";

import { GlobalStyles, FormsStyles } from "../styles";

import { UrlUserProfile } from "../services/constants";

export default class ProfileScreen extends React.Component {

    state = {
        username: "",
        name: "",
        email: "",
        cellphone: "",
        address: ""
    }

    componentDidMount() {
        this.getProfile();
    }

    getProfile = async () => {
        let url = UrlUserProfile + this.props.client;
        let datos = ''
        let response = await fetch(url, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(datos), // data can be `string` or {object}!
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).catch(error => console.error('Error:', error))
        .then(response => { return response; });
        if ( response.status == 'ok' ) {
            this.setState({
                username: response.data.user.username,
                name: response.data.persona.name,
                email: response.data.persona.email,
                cellphone: response.data.persona.cellphone,
                address: response.data.persona.address,
            });
        }
    }

    render() {
        return(
            <SafeAreaView style={GlobalStyles.container}>
                <View style={GlobalStyles.header}>
                    <Text style={GlobalStyles.sectionTitle}>User's Profile</Text>
                </View>
                <View style={GlobalStyles.body}>
                    <View style={GlobalStyles.topToolBar}>
                        <Button title="Edit" onPress={this.edita} />
                    </View>
                    <ScrollView style={GlobalStyles.scrollBody}>
                        <View style={GlobalStyles.infoFieldContainer}>
                            <Text style={GlobalStyles.infoFieldLabel}>Username</Text>
                            <Text style={GlobalStyles.infoFieldData}>{this.state.username}</Text>
                        </View>

                        <View style={GlobalStyles.infoFieldContainer}>
                            <Text style={GlobalStyles.infoFieldLabel}>Name</Text>
                            <Text style={GlobalStyles.infoFieldData}>{this.state.name}</Text>
                        </View>

                        <View style={GlobalStyles.infoFieldContainer}>
                            <Text style={GlobalStyles.infoFieldLabel}>E-Mail</Text>
                            <Text style={GlobalStyles.infoFieldData}>{this.state.email}</Text>
                        </View>

                        <View style={GlobalStyles.infoFieldContainer}>
                            <Text style={GlobalStyles.infoFieldLabel}>Cellphone</Text>
                            <Text style={GlobalStyles.infoFieldData}>{this.state.cellphone}</Text>
                        </View>

                        <View style={GlobalStyles.infoFieldContainer}>
                            <Text style={GlobalStyles.infoFieldLabel}>Address</Text>
                            <Text style={GlobalStyles.infoFieldData}>{this.state.address}</Text>
                        </View>
                    </ScrollView>
                </View>
                <View style={GlobalStyles.footer}>
                    <TouchableOpacity style={FormsStyles.bigButtonRed} onPress={this.props.handleLogout}>
                        <Text style={FormsStyles.textBigButton}>Logout</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        );
    }

}