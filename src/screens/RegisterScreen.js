import React from "react";

import { 
    Text, 
    View, 
    TextInput, 
    Alert, 
    KeyboardAvoidingView, 
    ScrollView,
    Keyboard,
    Platform,
    TouchableWithoutFeedback,
    TouchableOpacity,
    ActivityIndicator,
} from "react-native";

import { GlobalStyles, FormsStyles } from "../styles";

import { UrlRegisterUser } from "../services/constants";

export default class RegisterScreen extends React.Component {

    state = {
        message: "",
        name: "",
        email: "",
        password: "",
        passwordConfirm: "",
        cellphone: "",
        role: '4',
        client: '1',

        saving: false,
    }

    register = async () => {
        if ( this.validate() ) {
            this.setState( { saving: true });
            var data = this.state;
            let response = await fetch(UrlRegisterUser, {
                    method: 'POST', // or 'PUT'
                    body: JSON.stringify(data), // data can be `string` or {object}!
                    headers:{
                        'Content-Type': 'application/json'
                    }
            }).then(res => res.json()).catch(error => console.error('Error:', error)).then(response => {return response });
            
            if ( typeof(response) === 'object' ) {
                if ( response.status === 'ok' ) {
                    Alert.alert(
                        "Private Car Service",
                        "User created succesfully. Please clic OK to login.",
                        [
                          { text: "OK", onPress: () => this.props.handleRegistration() }
                        ],
                        { cancelable: false }
                    );
                }
                else {
                    let message = response.message + "\n\n";
                    if ( response.data !== null ) {
                        for (let i=0; i<response.data.length; i++ ) {
                            message += response.data[i] + "\n";
                        }
                    }
                    Alert.alert( "Private Car Service", "Cannot create your account. Please check details at top." );
                    this.setState({ message: message, saving: false });
                }
            }
            else {
                this.setState({message: 'Unable to reach server. Please contact manager for support, or visit http://'});  
                this.setState( { saving: false });  
            }
        }
    }

    validate = () => {
        return true;
    }

    render() {
        return(
            <KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : "height"} style={GlobalStyles.container}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={GlobalStyles.inner}>
                        <View style={GlobalStyles.header}>
                            <Text style={GlobalStyles.sectionTitle}>Creating new user</Text>
                        </View>
                        <View style={GlobalStyles.body}>
                            <ScrollView> 
                                <Text style={GlobalStyles.errorMessages}>{this.state.message}</Text>

                                <Text style={FormsStyles.fieldLabel}>Email (also will be your username)</Text>
                                <TextInput 
                                    placeholder="Email" 
                                    keyboardType="email-address"
                                    style={FormsStyles.input}
                                    returnKeyType="none"
                                    autoCapitalize = 'none'
                                    onChangeText={ (email) => { this.setState({email: email, username: email}) } } />

                                <Text style={FormsStyles.fieldLabel}>Password</Text>
                                <TextInput 
                                    placeholder="Password" 
                                    textContentType="password" 
                                    style={FormsStyles.input}
                                    returnKeyType="none"
                                    secureTextEntry={true}
                                    textContentType={'password'}
                                    onChangeText={ (password) => this.setState({password}) } />
                                
                                <Text style={FormsStyles.fieldLabel}>Confirm your password</Text>
                                <TextInput 
                                    placeholder="Password" 
                                    textContentType="password" 
                                    style={FormsStyles.input}
                                    returnKeyType="none"
                                    secureTextEntry={true}
                                    textContentType={'password'}
                                    onChangeText={ (passwordConfirm) => this.setState({passwordConfirm}) } />

                                <Text style={FormsStyles.fieldLabel}> Name</Text>
                                <TextInput 
                                    placeholder=" Name" 
                                    style={FormsStyles.input}
                                    returnKeyType="none"
                                    onChangeText={ (name) => this.setState({name}) } />
                                
                                <Text style={FormsStyles.fieldLabel}>Cell phone</Text>
                                <TextInput 
                                    placeholder="Cellphone" 
                                    autoCompleteType="tel"
                                    keyboardType="phone-pad"
                                    style={FormsStyles.input}
                                    onChangeText={ (cellphone) => this.setState({cellphone}) } />
                            </ScrollView>
                        </View>
                        {
                            (this.state.saving)?
                            <View style={GlobalStyles.footer}>
                                <View style={FormsStyles.bottomButtonsDisabled}>
                                    <ActivityIndicator />
                                    <Text style={FormsStyles.bottomButtonsText}>Creating Account</Text>
                                </View>
                            </View>
                            :
                            <View style={GlobalStyles.footer}>
                                <TouchableOpacity style={FormsStyles.bottomButtonsRed} onPress={this.props.handleBack} >
                                    <Text style={FormsStyles.bottomButtonsText}>Back</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={FormsStyles.bottomButtons} onPress={this.register}>
                                    <Text style={FormsStyles.bottomButtonsText}>Create Account</Text>
                                </TouchableOpacity>
                            </View>
                        }
                        
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        );
    }

}