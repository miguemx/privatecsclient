import React from "react";

import { 
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    View, 
    Text, 
    TextInput, 
    TouchableOpacity,
    Keyboard, 
    ScrollView,
    ActivityIndicator
} from "react-native";

import { GlobalStyles, FormsStyles } from "../styles";

class LoginScreen extends React.Component {

    state ={
        message: "",
        user: "",
        password: "",
        logingin: false,
    }

    login = async () => {
        this.setState({logingin: true});
        if ( this.state.user.length > 0 && this.state.password.length > 0 ) {
            await this.props.handleLogin(this.state.user, this.state.password);
            this.setState({logingin: false});
        }
        else {
            this.setState({
                message: "Plese enter your data",
                logingin: false,
            });
        }
    }

    render() {
        return(
            <KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : "height"} style={GlobalStyles.container}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={GlobalStyles.inner}>
                        <View style={GlobalStyles.header}>
                            <Text style={GlobalStyles.sectionTitle}>Private Car Service</Text>
                        </View>
                        <View style={GlobalStyles.body}>
                            <ScrollView contentContainerStyle={{alignItems: "center", padding: 20}}> 
                                <Text style={GlobalStyles.errorMessages}>{this.state.message}</Text>
                                <Text style={FormsStyles.fieldLabel}>Username</Text>
                                <TextInput 
                                    placeholder="Username" 
                                    style={FormsStyles.input}
                                    autoCapitalize = 'none'
                                    onChangeText={ (user) => this.setState({user}) } />

                                <Text style={FormsStyles.fieldLabel}>Password</Text>
                                <TextInput 
                                    placeholder="Password" 
                                    textContentType="password" 
                                    secureTextEntry={true} 
                                    style={FormsStyles.input}
                                    onChangeText={ (password) => this.setState({password}) } />
                                
                                {
                                    (this.state.logingin === false )?
                                    <TouchableOpacity onPress={this.login} style={FormsStyles.bigButton}>
                                        <Text style={FormsStyles.textBigButton}>Login</Text>
                                    </TouchableOpacity>
                                    :
                                    <View onPress={this.login} style={FormsStyles.bigButtonDisabled}>
                                        <ActivityIndicator />
                                        <Text style={FormsStyles.textBigButton}>Login</Text>
                                    </View>
                                }
                                
                                
                            </ScrollView>
                        </View>
                        
                        <View style={GlobalStyles.footerVertical}>
                            <Text style={GlobalStyles.normalText}>Don't have an account yet?</Text>
                            <TouchableOpacity style={FormsStyles.linkButton} onPress={this.props.signup}>
                                <Text style={FormsStyles.linkButtonText} >Click here</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        );
    }

}

export default LoginScreen;