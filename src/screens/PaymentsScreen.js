import React from "react";

import { 
    View, 
    Text, 
    SafeAreaView, 
    TouchableOpacity,
    Image,
    FlatList,
} from "react-native";

import { GlobalStyles } from "../styles";

import { UrlCards } from "../services/constants";
import { CardListItem, CreditCardEdit } from "../components";

export default class PaymentsScreen extends React.Component {

    state = {
        modalCardOpen: false,
        cards: []
    }

    componentDidMount() {
        this.getCards();
        const { navigation } = this.props;
        this.focusListener = navigation.addListener("focus", () => {      
            this.getCards();   
        });
    }

    /**
     * obtiene la lista de todas las tarjetas registradas por el cliente
     */
    getCards = async () => {
        let datos = null;
        let response = await fetch(UrlCards + this.props.client, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(datos), // data can be `string` or {object}!
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).catch(error => console.error('Error:', error))
        .then(response => { return response; });
        if ( typeof(response) === 'object' ) {
            if ( response.status === 'ok' ) {
                let cards = response.data.cards;
                this.setState({cards});
            }
            else {
                console.log( "no cards" );
            }
        }
    }

    /**
     * abre el formulario para agregar una nueva tarjeta
     */
    addCard = () => {
        let modalCardOpen = true;
        this.setState({ modalCardOpen });
    }

    dismissCard = () => {
        let modalCardOpen = false;
        this.setState({ modalCardOpen });
    }

    render() {
        return(
            <SafeAreaView style={GlobalStyles.container}>
                <View style={GlobalStyles.header}>
                    <Text style={GlobalStyles.sectionTitle}>Payment methods</Text>
                </View>
                <View style={GlobalStyles.body}>
                    <View style={GlobalStyles.topToolBar}>
                        <TouchableOpacity onPress={this.addCard}>
                            <Image source={require("../../assets/icons/credit-card-plus.png")} />
                        </TouchableOpacity>
                    </View>
                    {
                        (this.state.cards.length > 0)?
                        <FlatList
                            data={this.state.cards}
                            renderItem={ (item) => <CardListItem cardData={item.item} updateCards={this.getCards} client={this.props.client} />}
                            keyExtractor={item => item.id}
                        />
                        :
                        <View style={{color: "#fff", flex: 1, justifyContent: "center", alignItems:"center"}}>
                            <Text style={{color: "#fff"}}>You don't have cards registred yet.</Text>
                        </View>
                    }
                    <Text style={GlobalStyles.normalText}>
                        * Selected card will be used as default payment method. If no one is selected, 
                        first card in the list will be used as default card
                    </Text>
                    
                </View>

                <CreditCardEdit 
                    modalCardOpen={this.state.modalCardOpen} 
                    client={this.props.client} 
                    dismiss={this.dismissCard}
                    success={this.getCards} />
                
            </SafeAreaView>
        );
    }

}