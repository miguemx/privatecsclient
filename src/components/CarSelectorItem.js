import React from "react";

import {
    TouchableOpacity,
    Text,
    Image,
    View
} from "react-native";

import { IconCar, IconCarMinivan, IconCarSedan, IconCarSuv } from "../services/constants"

import { ServiceStyles } from "../styles";

export default class CarSelectorItem extends React.Component {


    /**
     * selecciona el icono para la lista desplegable
     */
    selectIcon = () => {
        let icon = "../../assets/icons/car.png";
        
    }

    /**
     * envia la seleccion del usuario basada en el ID del tipo de carro
     */
    clicItem = () => {
        this.props.select(this.props.carData.id);
    }

    render() {
        let style = (this.props.carData.id == this.props.selected)?ServiceStyles.carListItemSelected: ServiceStyles.carListItem;
        let styleText = (this.props.carData.id == this.props.selected)?ServiceStyles.carListNameSelected: ServiceStyles.carListName;
        let styleAmount = (this.props.carData.id == this.props.selected)?ServiceStyles.carListPriceSelected: ServiceStyles.carListPrice;
        let icon;
        switch( this.props.carData.id ) {
            case "1": icon = IconCar; break;
            case "2": icon = IconCarSedan; break;
            case "3": icon = IconCarMinivan; break;
            case "4": icon = IconCarSuv; break;
            default: icon = IconCar; break;
        }
        return(
            <TouchableOpacity style={style} onPress={this.clicItem}>
                <Image source={icon} />
                <Text style={styleText}>
                    {this.props.carData.name}{"\n"}
                    {this.props.carData.passanger} passanger
                </Text>
                
                <Text style={styleAmount}> $ {this.props.carData.amount}</Text>
            </TouchableOpacity>
        );
    }
}