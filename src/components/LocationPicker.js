import React from "react";

import { Modal, View, TouchableOpacity, ActivityIndicator, Text } from "react-native";

import MyMapView from "./MapView";
import MapInput from "./MapInput";
import { getInfoAddress } from "../services/address";
import { getLocation } from "../services/location-services";

import { ServiceStyles, FormsStyles } from "../styles"

export default class LocationPicker extends React.Component {

    state = {
        region: {}
    }

    componentDidMount() {
        this.getInitialState();
    }

    /**
     * obtiene la localizacion incial del dispositivo para ponerla en el mapa
     */
    getInitialState() {
        getLocation().then(
            (data) => {
                this.setState({
                    region: {
                        latitude: data.latitude,
                        longitude: data.longitude,
                        latitudeDelta: 0.003,
                        longitudeDelta: 0.003
                    }
                });
            }
        );
    }

    /**
     * ajusta las corrdenadas para la region actual
     * @param {object} loc la localizacion en cordenadas
     * @param {object} pid el ID de maps para el lugar seleccionado
     * @param {string} name el nombre de google maps
     * @param {array} address los componentes de la direccion
     */
    getCoordsFromName(loc, pid, name, address) {
        let addressData = getInfoAddress(address);
        this.setState({
            region: {
                latitude: loc.lat,
                longitude: loc.lng,
                latitudeDelta: 0.003,
                longitudeDelta: 0.003,
                placeId: pid, 
                name: name,
                number: addressData.streetNumber,
                street: addressData.street,
                city: addressData.city,
                state: addressData.state,
                zip: addressData.postalCode,
                country: addressData.country,
            }
        });
    }

    /**
     * Actualiza el mapa mostrado al lugar escrito
     * @param {object} region 
     */
    onMapRegionChange(region) {
        this.setState({ region });
    }

    /**
     * sets location for current picker window. Sends it to main component
     */
    setLocation = () => {
        this.props.setLocation( this.props.tipo, this.state.region );
    }


    render() {
        return(
            <Modal style={ServiceStyles.container}
                animationType="slide"
                transparent={false}
                visible={this.props.visible}>    
                {
                    this.state.region['latitude'] ?
                        <View style={ServiceStyles.mapSearcher}>
                            <MyMapView
                                region={this.state.region}
                                onRegionChange={(reg) => this.onMapRegionChange(reg)} />
                        </View> : null
                }
                <Text style={ServiceStyles.questionTop}>{this.props.title}</Text>
                <View style={ServiceStyles.searcher}>
                    <MapInput notifyChange={(loc, pid, name, address) => this.getCoordsFromName(loc, pid, name, address)}  />
                </View>
                <View style={ServiceStyles.tripsControls}>
                    <TouchableOpacity
                        style={FormsStyles.bigButton}
                        onPress={this.setLocation}>
                        <Text style={FormsStyles.textBigButton}>Set Location</Text>
                    </TouchableOpacity>
                </View>
            </Modal>
        );
    }

}