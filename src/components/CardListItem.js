import React from "react";

import {
    View,
    Text,
    TouchableOpacity,
    Image,
    Alert,
    ActivityIndicator,
} from "react-native";

import { GlobalStyles } from "../styles";

import { UrlCardDefault, UrlCardRemove } from "../services/constants";

export default class CardListItem extends React.Component {
    state = {
        deleting: false,
        defaulting: false,
    }

    /**
     * llama a la API para convertir una tarjeta en la principal del pago, pasando
     * el ID de la tarjeta que viene desde el componente, y el userId que
     * viene en las propiedades del componente
     */
    makeDefault = async () => {
        let datos = new Object();
        datos.userId = this.props.client;
        datos.cardId = this.props.cardData.id;
        let response = await fetch(UrlCardDefault, {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(datos), // data can be `string` or {object}!
                headers:{
                    'Content-Type': 'application/json'
                }
        }).then(res => res.json()).catch(error => console.error('Error:', error))
        .then(response => { return response; });

        if ( typeof(response) === 'object' ) {
            if ( response.status === 'ok' ) {
                this.props.updateCards();
            }
        }
    }

    /**
     * Abre el dialogo para remover la tarjeta de credito actual
     */
    remove = () => {
        Alert.alert(
            "Private Car Service",
            "Are you sure you want to remove this card?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { 
                    text: "OK", onPress: () => this.proccessRemove() 
                }
            ],
            { cancelable: false }
        );
      
    }

    /**
     * llama a la API para remover la tarjeta actual
     */
    proccessRemove = async () => {
        await this.setState({deleting: true});
        let datos = new Object();
        datos.userId = this.props.client;
        datos.cardId = this.props.cardData.id;
        let response = await fetch(UrlCardRemove, {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(datos), // data can be `string` or {object}!
                headers:{
                    'Content-Type': 'application/json'
                }
        }).then(res => res.json()).catch(error => console.error('Error:', error))
        .then(response => { return response; });

        if ( typeof(response) === 'object' ) {
            if ( response.status === 'ok' ) {
                this.props.updateCards();
            }
            else {
                Alert.alert(
                    "Private Car Service",
                    response.message,
                    [
                        { 
                            text: "OK", onPress: () => this.props.updateCards() 
                        }
                    ],
                    { cancelable: false }
                );
            }
        }
    }

    render() {
        return(
            <View style={GlobalStyles.basicItemContainer} >
                {
                    (this.props.cardData.default == "1")? 
                    <TouchableOpacity onPress={this.edit} style={GlobalStyles.basicItemInfoButton}>
                        <Image source={require("../../assets/icons/radiobox-marked.png")} />
                    </TouchableOpacity>
                    :
                    <TouchableOpacity onPress={this.makeDefault} style={GlobalStyles.basicItemInfoButton}>
                        <Image source={require("../../assets/icons/radiobox-blank.png")} />
                    </TouchableOpacity>
                }
                <View style={GlobalStyles.basicItemInfoContainer}>
                    <Text style={GlobalStyles.basicItemInfoTitle}>{this.props.cardData.cardNumber}</Text>
                    <Text style={GlobalStyles.basicItemInfoText}>
                        Exp: {this.props.cardData.expMonth} / {this.props.cardData.expYear} 
                    </Text>
                </View>
                {
                    (this.state.deleting === false)?
                    <TouchableOpacity onPress={this.remove} style={GlobalStyles.basicItemInfoButton}>
                        <Image source={require("../../assets/icons/credit-card-remove.png")} />
                    </TouchableOpacity>
                    :
                    <ActivityIndicator />
                }
                
            </View>
        );
    }
}