import React from "react";
import { ActivityIndicator } from "react-native";
import {
    Modal,
    View,
    Text,
    TouchableOpacity, Image
} from "react-native";
import { ServiceStyles } from "../styles";

export default class Calificador extends React.Component {

    state = {
        submiting: false,
    }

    /**
     * 
     * @param {int} score la calificacion que da el usuario
     */
    sendFeedback = (score) => {
        console.log(score);
        this.props.success();
    }

    render() {
        return(
            <Modal style={ServiceStyles.container}
                animationType="slide"
                transparent={false}
                visible={this.props.visible}
            >
                <View style={ServiceStyles.califContainer}>
                    <Text style={ServiceStyles.califQuestion}>How satisfied are you with your trip?</Text>
                    {
                        (this.state.submiting)?
                        <View style={ServiceStyles.califStarsContainer}><ActivityIndicator size="large" /></View>
                        :
                        <View style={ServiceStyles.califStarsContainer}>
                            <TouchableOpacity onPress={()=>this.sendFeedback(1)} style={ServiceStyles.califEmoji}>
                                <Image source={require("../../assets/icons/emoticon-angry-outline.png")} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={()=>this.sendFeedback(2)} style={ServiceStyles.califEmoji}>
                                <Image source={require("../../assets/icons/emoticon-sad-outline.png")} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={()=>this.sendFeedback(3)} style={ServiceStyles.califEmoji}>
                                <Image source={require("../../assets/icons/emoticon-neutral-outline.png")} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={()=>this.sendFeedback(4)} style={ServiceStyles.califEmoji}>
                                <Image source={require("../../assets/icons/emoticon-happy-outline.png")} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={()=>this.sendFeedback(5)} style={ServiceStyles.califEmoji}>
                                <Image source={require("../../assets/icons/emoticon-excited-outline.png")} />
                            </TouchableOpacity>
                        </View>
                    }
                </View>
                
            </Modal>
        );
    }

}