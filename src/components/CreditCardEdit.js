import React from "react";

import {
    Modal,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Image,
    ActivityIndicator,
    Alert,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard
} from "react-native";
import DropDownPicker from 'react-native-dropdown-picker';

import { Months, Years, UrlCardAdd } from "../services/constants";

import { GlobalStyles, FormsStyles } from "../styles";

export default class CreditCardEdit extends React.Component {

    state = {
        cardNumber: "",
        expMonth: "",
        expYear: "",
        cvc: "",
        name: "",
        saving: false,
        errors: ""
    }

    componentDidMount() {
    }

    save = async () => {
        console.log("saving card");
        console.log( this.state );
        this.setState({ saving: true, errors: "" });

        let datos = this.state;
        datos.userId = this.props.client;
        let response = await fetch(UrlCardAdd, {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(datos), // data can be `string` or {object}!
                headers:{
                    'Content-Type': 'application/json'
                }
        }).then(res => res.json()).catch(error => console.error('Error:', error))
        .then(response => { return response; });

        console.log(response);

        if ( typeof(response) === 'object' ) {
            if ( response.status === 'ok' ) {
                this.props.success();
                this.props.dismiss();
            }
            else {
                let errors = response.message;
                console.log( typeof(response.data) );
                if ( typeof(response.data) == 'object' && response.data != null ) {
                    for ( let i=0; i<response.data.length; i++ ) {
                        errors += "\n - " + response.data[i];
                    }
                }
                this.setState({ errors });
            }
        }

        this.setState({ saving: false });
    }

    render() {
        return(
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.props.modalCardOpen} >
                
                <KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : "height"} style={GlobalStyles.container}>
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <View style={GlobalStyles.inner}>
                            <View style={GlobalStyles.header}>
                                <Text style={GlobalStyles.sectionTitle}>Editing credit card</Text>
                            </View>
                            <View style={FormsStyles.formCardContainer}>
                                <View style={GlobalStyles.topToolBar}>
                                    {
                                        (this.state.saving === false ) ?
                                        <TouchableOpacity onPress={this.save}>
                                            <Image source={require("../../assets/icons/content-save.png")} />
                                        </TouchableOpacity>
                                        :
                                        <ActivityIndicator />
                                    }

                                    {
                                        (this.state.saving === false ) ?
                                        <TouchableOpacity onPress={ this.props.dismiss }>
                                            <Image source={require("../../assets/icons/close-circle.png")} />
                                        </TouchableOpacity>
                                        :
                                        <ActivityIndicator />
                                    }
                                    
                                    
                                </View>
                                <Text style={GlobalStyles.sectionTitle}>Card Details</Text>
                                <View style={FormsStyles.creditCardContainer}>
                                    <TextInput 
                                        placeholder="Name (as shown on card)" 
                                        style={FormsStyles.input}
                                        onChangeText={ (name) => { this.setState({ name }) }} />
                                    <TextInput 
                                        placeholder="Card Number" 
                                        style={FormsStyles.input}
                                        keyboardType="numeric"
                                        onChangeText={ (cardNumber) => { this.setState({ cardNumber }) } } />
                                    <View style={FormsStyles.creditCardDataContainer}>
                                        <View style={FormsStyles.creditCardData}>
                                            <TextInput 
                                                placeholder="Exp. Month" 
                                                style={FormsStyles.input}
                                                keyboardType="numeric"
                                                onChangeText={ (expMonth) => { this.setState({ expMonth }) } } />
                                        </View>
                                        <View style={FormsStyles.creditCardData}>
                                            <TextInput 
                                                placeholder="Exp. Year" 
                                                style={FormsStyles.input}
                                                keyboardType="numeric"
                                                onChangeText={ (expYear) => { this.setState({ expYear }) } } />
                                        </View>
                                        <View style={FormsStyles.creditCardData}>
                                            <TextInput 
                                                placeholder="CVC" 
                                                style={FormsStyles.input}
                                                keyboardType="numeric"
                                                secureTextEntry={true}
                                                onChangeText={ (cvc) => { this.setState({ cvc }) }} />
                                        </View>
                                    </View>
                                    <Text style={GlobalStyles.errorMessages}>{this.state.errors}</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
                
            </Modal>
        );
    }

}