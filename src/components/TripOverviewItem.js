import React from "react";

import {
    View,
    TouchableOpacity,
    Text,
    Image,
} from "react-native";

import { ServiceStyles } from "../styles";

export default class TripOverviewItem extends React.Component {

    openLocation = () => {
        this.props.openLocation( this.props.tipo, true );
    }

    removeLocation = () => {
        this.props.removeLocation( this.props.tipo );
    }

    render() {
        return(
            <View style={ServiceStyles.tripOverviewItem} >
                <View style={ServiceStyles.tripOverviewItemRow}>
                    <Text style={ServiceStyles.touchableInputLabel}>{this.props.title}:</Text>

                        <TouchableOpacity style={ServiceStyles.buttonItem} onPress={ this.removeLocation }>
                            <Image source={require("../../assets/icons/trash-can-outline-24.png")} />
                        </TouchableOpacity>
                </View>
                
                <TouchableOpacity style={ServiceStyles.touchableInput} onPress={ this.openLocation }>
                    <Text style={ServiceStyles.touchableInputText}>{this.props.inputText}</Text>
                </TouchableOpacity>
                
            </View>
        );
    }

}