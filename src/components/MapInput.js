import React from 'react';
import { FlatList } from 'react-native-gesture-handler';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

/**
 * Crea un map input para hacer una busqueda de lugares a traves de la API de google maps
 * IMPORTANTE: NUNCA SE DEBE PONER DENTRO DE UN SCROLL VIEW, INCLUSO AUNQUE SE TRATE DE UN MODAL
 * @param {object} props las propiedades del objeto map input
 */
function MapInput(props){
    // return (
    //     <GooglePlacesAutocomplete
    //         placeholder='Search.'
    //         minLength={2} // minimum length of text to search
    //         autoFocus={true}
    //         returnKeyType={'search'} // Can be left out for default return key 
    //         fetchDetails={true}
    //         onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
    //             props.notifyChange(details.geometry.location, details.place_id, details.name, details.address_components);
    //         }}
    //         query={{
    //             key: 'AIzaSyCCrxXLnGg2BkZn8oxS0b6dgwe5zB8RnyU',
    //             language: 'en'
    //         }}
    //         nearbyPlacesAPI='GooglePlacesSearch'
    //         debounce={300}
    //         onSelect={ ( { description } ) => (
    //             console.log(description)
    //           )}
    //         style={{zIndex: 10000}}
    //         listEmptyComponent={listaVacia}
    //     />
    // );
    return <GooglePlacesAutocomplete
      placeholder='Search'
      onPress={(data, details = null) => {
        // 'details' is provided when fetchDetails = true
        console.log(data, details);
      }}
      query={{
        key: 'AIzaSyDibRTaMlFA1f-Wj7G5a53BJNLtNTbENVU',
        language: 'en',
      }}
    />
}


function listaVacia() {
    let items = []
    return <FlatList items={items} />
}
export default MapInput;