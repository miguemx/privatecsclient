import React from "react";

import {
    FlatList
} from "react-native";
import CarSelectorItem from "./CarSelectorItem";

export default class CarSelector extends React.Component {

    setCarType = (idCarType) => {
        this.props.selectCarType(idCarType);
    }

    render() {
        return(
            <FlatList
                data={this.props.cars}
                renderItem={ (item) => <CarSelectorItem carData={item.item} select={this.setCarType} selected={this.props.selected} />}
                keyExtractor={item => item.name}
            />
        );
    }
}